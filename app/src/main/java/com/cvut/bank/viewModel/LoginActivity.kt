package com.cvut.bank.viewModel

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.biometric.BiometricPrompt
import androidx.core.content.ContextCompat
import com.cvut.bank.databinding.ActivityLoginBinding
import com.cvut.bank.service.UserService
import java.util.concurrent.Executor

class LoginActivity : AppCompatActivity() {
    private lateinit var userService: UserService
    private lateinit var binding: ActivityLoginBinding
    private lateinit var executor: Executor
    private lateinit var biometricPrompt: BiometricPrompt
    private lateinit var promptInfo: BiometricPrompt.PromptInfo
    private lateinit var sharedPreferences: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        userService = UserService(context = this)
        binding = ActivityLoginBinding.inflate(layoutInflater)

        setContentView(binding.root)
        supportActionBar?.hide()
        sharedPreferences = getSharedPreferences("MySharedPref", Context.MODE_PRIVATE)

        executor = ContextCompat.getMainExecutor(this)
        biometricPrompt = BiometricPrompt(this, executor, object : BiometricPrompt.AuthenticationCallback() {
            override fun onAuthenticationError(errorCode: Int, errString: CharSequence) {
                super.onAuthenticationError(errorCode, errString)
                Toast.makeText(applicationContext, "Nepodařilo se přihlásit", Toast.LENGTH_SHORT).show()
            }

            override fun onAuthenticationSucceeded(result: BiometricPrompt.AuthenticationResult) {
                super.onAuthenticationSucceeded(result)
                val userId = sharedPreferences.getString("userId", null)

                val intent = Intent(this@LoginActivity, WelcomePageActivity::class.java)
                intent.putExtra("userId", userId)
                startActivity(intent)
                finish()
            }

            override fun onAuthenticationFailed() {
                super.onAuthenticationFailed()
                Toast.makeText(applicationContext, "Špatný email nebo heslo", Toast.LENGTH_SHORT).show()
            }
        })

        promptInfo = BiometricPrompt.PromptInfo.Builder()
            .setTitle("Biometrické přihlášení do aplikace")
            .setNegativeButtonText(getString(android.R.string.no))
            .build()

        val isLoggedIn = sharedPreferences.getBoolean("isLoggedIn", false)
        val useFingerprintLogin = sharedPreferences.getBoolean("useFingerprintLogin", false)
        if (isLoggedIn && useFingerprintLogin) {
            biometricPrompt.authenticate(promptInfo)
        } else {
            binding.loginButton.setOnClickListener {
                val email = binding.textEmailAddress.text.toString()
                val password = binding.textPassword.text.toString()

                if (email.isEmpty()) {
                    binding.textEmailAddress.error = "Email nesmí být prázdný."
                    binding.textEmailAddress.requestFocus()
                    return@setOnClickListener
                }

                if (password.isEmpty()) {
                    binding.textPassword.error = "Heslo nesmí být prázdné."
                    binding.textPassword.requestFocus()
                    return@setOnClickListener
                }

                userService.loginUser(email, password)
                    .addOnCompleteListener(this) { task ->
                        if (task.isSuccessful) {

                            val rememberMeChecked = binding.rememberMeCheckBox.isChecked
                            if (rememberMeChecked) {
                                val editor = sharedPreferences.edit()
                                editor.putBoolean("isLoggedIn", true)

                                val userId = userService.getAuthenticatedUserId()
                                editor.putString("userId", userId)
                                editor.apply()
                            }
                            val userId = userService.getAuthenticatedUserId()
                            val intent = Intent(this, WelcomePageActivity::class.java)
                            intent.putExtra("userId", userId)
                            startActivity(intent)
                            finish()
                        } else {
                            Toast.makeText(
                                baseContext, "Špatný email nebo heslo.",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
            }
            binding.registerButton.setOnClickListener {
                val intent = Intent(this, RegisterActivity::class.java)
                startActivity(intent)
            }
        }
    }
}