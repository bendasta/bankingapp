package com.cvut.bank.viewModel

import android.content.Intent
import android.os.Bundle
import android.widget.ImageButton
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.cvut.bank.R
import com.cvut.bank.databinding.ActivityRegisterBinding
import com.cvut.bank.service.UserService
import com.cvut.bank.service.ValidationService

class RegisterActivity : AppCompatActivity() {
    private lateinit var binding: ActivityRegisterBinding
    private lateinit var userService: UserService
    private lateinit var validationService: ValidationService

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        userService = UserService(context = this)
        validationService = ValidationService()
        binding = ActivityRegisterBinding.inflate(layoutInflater)

        setContentView(binding.root)
        supportActionBar?.hide()

        val backButton: ImageButton = findViewById(R.id.back_button)
        backButton.setOnClickListener {
            finish()
        }

        binding.registerButton.setOnClickListener{
            val name = binding.textRegisterName.text.toString()
            val surname = binding.textRegisterSurname.text.toString()
            val email = binding.textRegisterEmail.text.toString()
            val password = binding.textRegisterPassword.text.toString()
            val confirmPassword = binding.textRegisterConfirmPassword.text.toString()

            if (!validationService.isNotEmpty(name)) {
                binding.textRegisterName.error = "Jméno je vyžadováno"
                binding.textRegisterName.requestFocus()
            }
            else if (!validationService.isNotEmpty(surname)) {
                binding.textRegisterSurname.error = "Příjmení je vyžadováno"
                binding.textRegisterSurname.requestFocus()
            }
            else if (!validationService.isNotEmpty(email)) {
                binding.textRegisterEmail.error = "Email je povinný"
                binding.textRegisterEmail.requestFocus()
            }
            else if (!validationService.isNotEmpty(password)) {
                binding.textRegisterPassword.error = "Heslo je povinné"
                binding.textRegisterPassword.requestFocus()
            }
            else if (!validationService.isNotEmpty(confirmPassword)) {
                binding.textRegisterConfirmPassword.error = "Potvrzení hesla je povinné"
                binding.textRegisterConfirmPassword.requestFocus()
            }
            else if (password != confirmPassword) {
                binding.textRegisterConfirmPassword.error = "Hesla se neshodují"
                binding.textRegisterConfirmPassword.requestFocus()
            }
            else if (!validationService.isValidEmail(email)) {
                binding.textRegisterEmail.error = "Email není ve správném formátu"
                binding.textRegisterEmail.requestFocus()
            }
            else if (!validationService.isValidPassword(password)) {
                binding.textRegisterPassword.error = "Heslo musí obsahovat alespoň 8 znaků, jedno velké písmeno, jedno malé písmeno, jedno číslo a jeden speciální znak"
                binding.textRegisterPassword.requestFocus()
            }
            else {
                userService.createUser(name, surname, email, password)
                    .addOnCompleteListener(this) { task ->
                        if (task.isSuccessful) {
                            Toast.makeText(baseContext, "Registration succeeded.",
                                Toast.LENGTH_SHORT).show()
                            val intent = Intent(this, LoginActivity::class.java)
                            startActivity(intent)
                            finish()
                        } else {
                            Toast.makeText(baseContext, "Registration failed.",
                                Toast.LENGTH_SHORT).show()
                        }
                    }
            }
        }
    }
}