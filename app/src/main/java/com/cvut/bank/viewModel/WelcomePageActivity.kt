package com.cvut.bank.viewModel

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.ContentValues.TAG
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.Button
import android.widget.FrameLayout
import android.widget.ImageView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.cvut.bank.R
import com.cvut.bank.adapters.WatchlistAccountsAdapter
import com.cvut.bank.databinding.ActivityWelcomePageBinding
import com.cvut.bank.model.Account
import com.cvut.bank.model.FinancialProduct
import com.cvut.bank.model.Investment
import com.cvut.bank.model.Loan
import com.cvut.bank.model.SavingsAccount
import com.cvut.bank.model.User
import com.cvut.bank.service.UserService
import kotlin.reflect.KClass

class WelcomePageActivity : BaseActivity(),WatchButtonStateProvider {
    private lateinit var binding: ActivityWelcomePageBinding
    private val userService = UserService(this)
    private val accountTypes: Map<String, KClass<out FinancialProduct>> = mapOf(
        "Account" to Account::class,
        "Savings" to SavingsAccount::class,
        "Loan" to Loan::class,
        "Investment" to Investment::class
    )

    private lateinit var watchlistRecyclerView: RecyclerView
    private var isWatchButtonClicked = false

    private lateinit var profileImage: ImageView

    @SuppressLint("NotifyDataSetChanged", "SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityWelcomePageBinding.inflate(layoutInflater)

        findViewById<FrameLayout>(R.id.activity_content).addView(binding.root)
        supportActionBar?.hide()

        watchlistRecyclerView = findViewById(R.id.watchlist_recycler_view)
        watchlistRecyclerView.layoutManager = LinearLayoutManager(this)

        profileImage = findViewById(R.id.profile_image)

        val watchButton: Button = findViewById(R.id.watchButton)
        watchButton.setOnClickListener {
            val plusButton: Button = findViewById(R.id.plusButton)
            val animIn = AnimationUtils.loadAnimation(this, R.anim.scale_up)
            plusButton.startAnimation(animIn)
            plusButton.visibility = View.VISIBLE
            isWatchButtonClicked = !isWatchButtonClicked
            watchlistRecyclerView.adapter?.notifyDataSetChanged()
            if (!isWatchButtonClicked) {
                val animOut = AnimationUtils.loadAnimation(this, R.anim.scale_down)
                plusButton.startAnimation(animOut)
                plusButton.visibility = View.GONE
            }
        }

        val userId = intent.getStringExtra("userId")
        if (userId != null) {
            userService.getUser(userId).addOnSuccessListener { document ->
                if (document != null) {
                    val user = document.toObject(User::class.java)


                    if (user?.name?.last() == 'a') {
                        binding.textViewUserName.text = "Vítejte, ${addNameEnding(user.name.toString())}"
                    } else {
                        if (user != null) {
                            binding.textViewUserName.text = "Vítej, ${addNameEnding(user.name.toString())}"
                        }
                    }

                    Glide.with(this)
                        .load(user?.profileImageUrl ?: R.drawable.default_profile_image)
                        .circleCrop()
                        .into(profileImage)

                    userService.updateUserAccountsFromBankApi(userId)

                    user?.financialProductsRefs?.forEach { financialProductRef ->
                        financialProductRef.get().addOnSuccessListener { documentSnapshot ->
                            val accountType = documentSnapshot.getString("accountType")
                            val accountTypeClass = accountTypes[accountType]
                            if (accountTypeClass != null) {
                                val specificFinancialProduct = documentSnapshot.toObject(accountTypeClass.java)
                                if (specificFinancialProduct is FinancialProduct) {
                                    user.financialProducts.add(specificFinancialProduct)
                                }
                            }
                        }
                    }
                    user?.watchlistRefs?.forEach { accountRef ->
                        accountRef.get().addOnSuccessListener { documentSnapshot ->
                            val accountType = documentSnapshot.getString("accountType")
                            val accountTypeClass = accountTypes[accountType]
                            if (accountTypeClass != null) {
                                val specificFinancialProduct =
                                    documentSnapshot.toObject(accountTypeClass.java)
                                if (specificFinancialProduct is FinancialProduct) {
                                    user.watchlist.add(specificFinancialProduct)
                                }
                            }
                        }.addOnCompleteListener {

                            watchlistRecyclerView.adapter?.notifyDataSetChanged()
                        }
                    }

                    val watchlistAccounts = user?.watchlist
                    if (watchlistAccounts != null) {
                        watchlistRecyclerView.adapter = WatchlistAccountsAdapter(watchlistAccounts, userService.getUser(userId),this)
                        watchlistRecyclerView.addItemDecoration(SpaceItemDecoration(15))
                    }
                } else {
                    Log.d(TAG, "Takový dokument neexistuje.")
                }
            }.addOnFailureListener { exception ->
                Log.d(TAG, "TODO", exception)
            }

        }

        val sharedPreferences = getSharedPreferences("MySharedPref", Context.MODE_PRIVATE)
        val isFirstLogin = sharedPreferences.getBoolean("isFirstLogin", true)
        if (isFirstLogin) {
            AlertDialog.Builder(this)
                .setTitle("Přihlašení otiskem prstu")
                .setMessage("Chcete použít otisk prstu pro přihlášení?")
                .setPositiveButton("Yes") { _, _ ->
                    val editor = sharedPreferences.edit()
                    editor.putBoolean("useFingerprintLogin", true)
                    editor.putBoolean("isFirstLogin", false)
                    editor.apply()
                }
                .setNegativeButton("No") { _, _ ->
                    val editor = sharedPreferences.edit()
                    editor.putBoolean("useFingerprintLogin", false)
                    editor.putBoolean("isFirstLogin", false)
                    editor.apply()
                }
                .show()
        }


        val plusButton: Button = findViewById(R.id.plusButton)
        plusButton.visibility = View.GONE
        plusButton.setOnClickListener {
            val intent = Intent(this, ListAccountsActivity::class.java)
            intent.putExtra("userId", userId)
            intent.putExtra("isPlusButtonClicked", true)
            intent.putExtra("selectedItemId", R.id.navigation_account)
            intent.putExtra("showBackButton", true)
            startActivity(intent)
        }
    }

    private fun addNameEnding(name: String): String {
        return when (name.last()) {
            'r', 'n', 'l', 't', 'd', 'x' -> "$name${'e'}"
            'k', 'g', 'h', 'c', 'j', 'q', 'w', 'p', 's', 'v', 'z' -> "$name${'u'}"
            'b', 'f', 'm' -> "$name${'e'}"
            'a' -> name.replaceRange(name.lastIndex, name.length, "o")
            else -> name
        }
    }

    override fun isWatchButtonClicked(): Boolean {
        return isWatchButtonClicked
    }
}