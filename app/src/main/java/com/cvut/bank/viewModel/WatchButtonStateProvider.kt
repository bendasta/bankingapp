package com.cvut.bank.viewModel

interface WatchButtonStateProvider {
    fun isWatchButtonClicked(): Boolean
}