package com.cvut.bank.viewModel

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.widget.Button
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.bumptech.glide.Glide
import com.cvut.bank.R
import com.cvut.bank.databinding.ActivityUserProfileBinding
import com.cvut.bank.model.User
import com.cvut.bank.service.UserService
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.storage.FirebaseStorage
import java.util.UUID

class UserProfileActivity : BaseActivity() {

    private lateinit var binding: ActivityUserProfileBinding
    private lateinit var userName: TextView
    private lateinit var userSurname: TextView
    private lateinit var userEmail: TextView
    private lateinit var profileImage: ImageView
    private lateinit var documentsButton: Button
    private lateinit var logoutButton: Button
    private lateinit var sharedPreferences: SharedPreferences
    private val userService = UserService(this)


    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityUserProfileBinding.inflate(layoutInflater)

        findViewById<FrameLayout>(R.id.activity_content).addView(binding.root)
        supportActionBar?.hide()

        val selectedItemId = intent.getIntExtra("selectedItemId", 0)
        logoutButton = findViewById(R.id.logout_button)
        sharedPreferences = getSharedPreferences("MySharedPref", MODE_PRIVATE)

        val navView: BottomNavigationView = findViewById(R.id.nav_view)
        navView.selectedItemId = selectedItemId

        userName = findViewById(R.id.user_name)
        userSurname = findViewById(R.id.user_surname)
        userEmail = findViewById(R.id.user_email)
        profileImage = findViewById(R.id.profile_image)
        documentsButton = findViewById(R.id.documents_button)

        val userId = intent.getStringExtra("userId")
        if (userId != null) {
            userService.getUser(userId).addOnSuccessListener { documentSnapshot ->
                val user = documentSnapshot.toObject(User::class.java)
                userName.text = "${userName.text} ${user?.name}"
                userSurname.text = "${userSurname.text} ${user?.surname}"
                userEmail.text = "${userEmail.text} ${user?.email}"
                profileImage = findViewById(R.id.profile_image)

                Glide.with(this)
                    .load(user?.profileImageUrl ?: R.drawable.default_profile_image)
                    .circleCrop()
                    .into(profileImage)
            }
        }


        profileImage.setOnClickListener {
            showImageOptionsDialog()
        }


        documentsButton.setOnClickListener {
            val intent = Intent(this, DocumentsActivity::class.java)
            intent.putExtra("userId", userId)
            startActivity(intent)
        }

        logoutButton.setOnClickListener {
            logoutUser()
        }
    }
    private fun showImageOptionsDialog() {
        val options = arrayOf("Nahrát nový obrázek", "Smazat obrázek")
        AlertDialog.Builder(this)
            .setTitle("Upravit profilový obrázek")
            .setItems(options) { _, which ->
                when (which) {
                    0 -> uploadNewProfileImage()
                    1 -> deleteProfileImage()
                }
            }
            .show()
    }

    private fun uploadNewProfileImage() {
        val intent = Intent(Intent.ACTION_OPEN_DOCUMENT)
        intent.addCategory(Intent.CATEGORY_OPENABLE)
        intent.type = "image/*"
        startActivityForResult(intent, PICK_IMAGE_REQUEST)
    }

    @Deprecated("Deprecated in Java")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.data != null) {
            val imageUri = data.data!!

            val storageReference = FirebaseStorage.getInstance().getReference("profile_images/${UUID.randomUUID()}")
            storageReference.putFile(imageUri)
                .addOnSuccessListener {
                    storageReference.downloadUrl.addOnSuccessListener { imageUrl ->
                        val firebaseUser = FirebaseAuth.getInstance().currentUser
                        if (firebaseUser != null) {
                            userService.getUser(firebaseUser.uid).addOnSuccessListener { documentSnapshot ->
                                val user = documentSnapshot.toObject(User::class.java)
                                if (user != null) {
                                    user.profileImageUrl = imageUrl.toString()
                                    userService.updateUser(user)
                                    recreate()
                                }
                            }
                        }
                    }
                }
                .addOnFailureListener {
                    Toast.makeText(this, "Nahrání obrázku selhalo", Toast.LENGTH_SHORT).show()
                }
        }
    }

    private fun deleteProfileImage() {
        val firebaseUser = FirebaseAuth.getInstance().currentUser

        if (firebaseUser != null) {
            userService.getUser(firebaseUser.uid).addOnSuccessListener { documentSnapshot ->
                val user = documentSnapshot.toObject(User::class.java)
                if (user?.profileImageUrl != null) {
                    val storageReference = FirebaseStorage.getInstance().getReferenceFromUrl(user.profileImageUrl!!)
                    storageReference.delete()
                        .addOnSuccessListener {
                            user.profileImageUrl = null
                            userService.updateUser(user)
                            recreate()
                        }
                        .addOnFailureListener {
                            Toast.makeText(this, "Smazání obrázku selhalo", Toast.LENGTH_SHORT).show()
                        }
                }
            }
        }
    }

    private fun logoutUser() {
        FirebaseAuth.getInstance().signOut()

        val editor = sharedPreferences.edit()
        editor.putBoolean("isLoggedIn", false)
        editor.putBoolean("useFingerprintLogin", false)
        editor.putBoolean("isFirstLogin", true)
        editor.apply()

        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
        finish()
    }

    companion object {
        private const val PICK_IMAGE_REQUEST = 1
    }
}