package com.cvut.bank.viewModel

import android.os.Bundle
import android.view.View
import android.widget.ImageButton
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.cvut.bank.R
import com.cvut.bank.adapters.AssetAdapter
import com.cvut.bank.adapters.TransactionAdapter
import com.cvut.bank.model.Account
import com.cvut.bank.model.FinancialProduct
import com.cvut.bank.model.Investment
import com.cvut.bank.model.Loan
import com.cvut.bank.model.SavingsAccount
import com.cvut.bank.service.UserService
import com.google.firebase.firestore.FirebaseFirestore

class AccountDetailActivity : BaseActivity() {
    private lateinit var userService: UserService

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_account_detail)

        supportActionBar?.hide()

        userService = UserService(this)

        val backButton: ImageButton = findViewById(R.id.back_button)
        backButton.setOnClickListener {
            finish()
        }

        val accountNumber = intent.getStringExtra("account_number")
        if (accountNumber != null) {
            val db = FirebaseFirestore.getInstance()
            db.collection("accounts").document(accountNumber).get().addOnSuccessListener { documentSnapshot ->
                val accountType = documentSnapshot.getString("accountType")
                val account: FinancialProduct = when (accountType) {
                    "Investment" -> documentSnapshot.toObject(Investment::class.java)!!
                    "Savings" -> documentSnapshot.toObject(SavingsAccount::class.java)!!
                    "Loan" -> documentSnapshot.toObject(Loan::class.java)!!
                    "Account" -> documentSnapshot.toObject(Account::class.java)!!
                    else -> documentSnapshot.toObject(Account::class.java)!!
                }
                val accountNumberTextView: TextView = findViewById(R.id.account_detail_number)
                val accountBalanceTextView: TextView = findViewById(R.id.account_detail_balance)
                val bankNameTextView: TextView = findViewById(R.id.bank_name_detail)

                accountNumberTextView.text = account.accountNumber.toString()
                when (account) {
                    is Investment -> {
                        bankNameTextView.text = account.bank_name + "-Investice"
                        accountBalanceTextView.text = "Investovaný zůstatek " + account.investmentAmount.toString() + " Kč"
                    }

                    is Loan -> {
                        bankNameTextView.text = account.bank_name + "-Půjčka"
                        val remainingMonthsTextView: TextView = findViewById(R.id.loan_remaining_months)
                        val interestRateTextView: TextView = findViewById(R.id.loan_interest_rate)
                        val monthlyPaymentTextView: TextView = findViewById(R.id.loan_monthly_payment)

                        remainingMonthsTextView.text = "Do: ${account.until}"
                        interestRateTextView.text = "Úrok: ${account.interestRate}%"
                        monthlyPaymentTextView.text = "Měsíční platba: ${account.monthlyPayment} Kč"
                        accountBalanceTextView.text = "Půjčená částka " + account.loanAmount.toString() + " Kč"


                        remainingMonthsTextView.visibility = View.VISIBLE
                        interestRateTextView.visibility = View.VISIBLE
                        monthlyPaymentTextView.visibility = View.VISIBLE
                    }

                    is SavingsAccount -> {
                        bankNameTextView.text = account.bank_name + "-Spoření"
                        val interestRateTextView: TextView = findViewById(R.id.savings_interest_rate)

                        interestRateTextView.text = "Úrok: ${account.interestRate}%"
                        accountBalanceTextView.text = "Zůstatek " + account.balance.toString() + " Kč"

                        interestRateTextView.visibility = View.VISIBLE
                    }

                    else -> {
                        bankNameTextView.text = account.bank_name + "-Osobní účet"
                        accountBalanceTextView.text = "Zůstatek " + account.balance.toString() + " Kč"
                    }
                }
                if (account is Investment) {
                    val recyclerView: RecyclerView = findViewById(R.id.recycler_view_detail)
                    recyclerView.layoutManager = LinearLayoutManager(this)
                    recyclerView.adapter = AssetAdapter(account.assets)
                    recyclerView.addItemDecoration(SpaceItemDecoration(15))
                } else {
                    val recyclerView: RecyclerView = findViewById(R.id.recycler_view_detail)
                    recyclerView.layoutManager = LinearLayoutManager(this)
                    recyclerView.adapter = TransactionAdapter(account.transactions)
                    recyclerView.addItemDecoration(SpaceItemDecoration(15))
                }
            }.addOnFailureListener { exception ->
               exception.printStackTrace()
               Toast.makeText(this, "Nepodařilo se načíst údaje účtu", Toast.LENGTH_SHORT).show()
            }
        }
    }
}