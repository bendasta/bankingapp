package com.cvut.bank.viewModel

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.cvut.bank.R
import com.cvut.bank.service.UserService
import com.google.android.material.bottomnavigation.BottomNavigationView

open class BaseActivity : AppCompatActivity() {
    private lateinit var userService: UserService
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_base)

        userService = UserService(this)
        val userId = userService.getAuthenticatedUserId()

        val navView: BottomNavigationView = findViewById(R.id.nav_view)

        navView.setOnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.navigation_add_account -> {
                    if (this !is AddAccountActivity) {
                        val intent = Intent(this, AddAccountActivity::class.java)
                        intent.putExtra("selectedItemId", R.id.navigation_add_account)
                        startActivity(intent)
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                        finish()
                    }
                    true
                }
                R.id.navigation_home -> {
                    if (this !is WelcomePageActivity) {
                        val intent = Intent(this, WelcomePageActivity::class.java)
                        intent.putExtra("selectedItemId", R.id.navigation_home)
                        intent.putExtra("userId", userId)
                        startActivity(intent)
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                        finish()
                    }
                    true
                }
                R.id.navigation_account -> {
                    if (this !is ListAccountsActivity) {
                        val intent = Intent(this, ListAccountsActivity::class.java)
                        intent.putExtra("selectedItemId", R.id.navigation_account)
                        intent.putExtra("userId", userId)
                        startActivity(intent)
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                        finish()
                    }
                    true
                }
                R.id.navigation_profile -> {
                    if (this !is UserProfileActivity) {
                        val intent = Intent(this, UserProfileActivity::class.java)
                        intent.putExtra("selectedItemId", R.id.navigation_profile)
                        intent.putExtra("userId", userId)
                        startActivity(intent)
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                        finish()
                    }
                    true
                }
                else -> false
            }
        }
    }
}