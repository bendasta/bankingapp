package com.cvut.bank.viewModel

import android.os.Bundle
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageButton
import android.widget.ImageView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.cvut.bank.R
import com.cvut.bank.adapters.AccountsAdapter
import com.cvut.bank.databinding.ActivityListAccountsBinding
import com.cvut.bank.model.Account
import com.cvut.bank.model.FinancialProduct
import com.cvut.bank.model.Investment
import com.cvut.bank.model.Loan
import com.cvut.bank.model.SavingsAccount
import com.cvut.bank.model.User
import com.cvut.bank.service.UserService
import com.google.android.material.bottomnavigation.BottomNavigationView

class ListAccountsActivity : BaseActivity() {
    private lateinit var binding: ActivityListAccountsBinding
    private lateinit var userService: UserService
    private lateinit var accountsRecyclerView: RecyclerView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityListAccountsBinding.inflate(layoutInflater)

        val isPlusButtonClicked = intent.getBooleanExtra("isPlusButtonClicked", false)
        val userId = intent.getStringExtra("userId")
        findViewById<FrameLayout>(R.id.activity_content).addView(binding.root)

        val showBackButton = intent.getBooleanExtra("showBackButton", false)
        if (showBackButton) {
            val backButton: ImageButton = findViewById(R.id.back_button)
            backButton.visibility = View.VISIBLE
            backButton.setOnClickListener {
                backButton.visibility = View.GONE
                finish()
            }
        }

        supportActionBar?.hide()


        val selectedItemId = intent.getIntExtra("selectedItemId", 0)

        val navView: BottomNavigationView = findViewById(R.id.nav_view)
        navView.selectedItemId = selectedItemId

        userService = UserService(context = this)
        accountsRecyclerView = findViewById(R.id.accounts_recycler_view)
        accountsRecyclerView.layoutManager = LinearLayoutManager(this)
        accountsRecyclerView.addItemDecoration(SpaceItemDecoration(15))

        if (userId != null) {
            userService.updateUserAccountsFromBankApi(userId)
        }

        val profileImage: ImageView = findViewById(R.id.profile_image)

        if (userId != null) {
            userService.getUser(userId).addOnSuccessListener { document ->
                if (document != null) {
                    val user = document.toObject(User::class.java)

                    if (user != null) {
                        Glide.with(this)
                            .load(user.profileImageUrl ?: R.drawable.default_profile_image)
                            .circleCrop()
                            .into(profileImage)
                    }
                }
            }
        }

        userService.getUserAccounts().addOnSuccessListener { querySnapshot ->
            val accounts = querySnapshot.documents.mapNotNull { document ->
                val accountType = document.getString("accountType")
                when (accountType) {
                    "Account" -> document.toObject(Account::class.java)
                    "Investment" -> document.toObject(Investment::class.java)
                    "Savings" -> document.toObject(SavingsAccount::class.java)
                    "Loan" -> document.toObject(Loan::class.java)
                    else -> null
                }
            }
            accountsRecyclerView.adapter = AccountsAdapter(accounts, isPlusButtonClicked, userService.getUser(userId?:""))
        }
    }
}