package com.cvut.bank.viewModel

import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.FrameLayout
import android.widget.Toast
import com.cvut.bank.R
import com.cvut.bank.databinding.ActivityAddAccountBinding
import com.cvut.bank.model.FinancialProduct
import com.cvut.bank.service.BankService
import com.cvut.bank.service.CSOBServiceImpl
import com.cvut.bank.service.CoinbaseServiceImpl
import com.cvut.bank.service.GeorgeServiceImpl
import com.cvut.bank.service.KBServiceImpl
import com.cvut.bank.service.UserService
import com.cvut.bank.service.ValidationService
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.FirebaseFirestore

class AddAccountActivity : BaseActivity() {
    private lateinit var binding: ActivityAddAccountBinding
    private lateinit var validationService: ValidationService
    private lateinit var userService: UserService
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAddAccountBinding.inflate(layoutInflater)
        validationService = ValidationService()
        userService = UserService(this)

        findViewById<FrameLayout>(R.id.activity_content).addView(binding.root)

        supportActionBar?.hide()

        val selectedItemId = intent.getIntExtra("selectedItemId", 0)

        val navView: BottomNavigationView = findViewById(R.id.nav_view)
        navView.selectedItemId = selectedItemId


        val serviceProviders = arrayOf("Coinbase", "KB", "George", "ČSOB")
        val adapter = ArrayAdapter(this, R.layout.spinner_item, serviceProviders)
        binding.spinnerServiceProviders.adapter = adapter

        binding.buttonAddService.setOnClickListener {
            val accountNumber = binding.editTextAccountNumber.text.toString()
            val serviceProvider = binding.spinnerServiceProviders.selectedItem.toString()

            if (!validationService.isNotEmpty(accountNumber) || !validationService.isNotEmpty(serviceProvider)) {
                return@setOnClickListener
            }

            val validServiceProviders = arrayOf("Coinbase", "KB", "George", "ČSOB")
            if (!validationService.isValidAccountNumber(accountNumber) || !validationService.isValidServiceProvider(serviceProvider, validServiceProviders)) {
                Toast.makeText(this, "Zadejte platné údaje", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            val userId = userService.getAuthenticatedUserId() ?: return@setOnClickListener

            userService.addAccount(userId, accountNumber, serviceProvider).addOnSuccessListener {
                Toast.makeText(this, "Účet byl úspěšně přidán", Toast.LENGTH_SHORT).show()
                binding.editTextAccountNumber.text.clear()
                binding.spinnerServiceProviders.setSelection(0)
            }.addOnFailureListener { exception ->
                Toast.makeText(this, exception.message, Toast.LENGTH_SHORT).show()
            }
        }
    }

}