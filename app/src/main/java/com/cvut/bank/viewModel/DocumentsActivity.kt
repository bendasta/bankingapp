package com.cvut.bank.viewModel

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.widget.Button
import android.widget.ImageButton
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.cvut.bank.R
import com.cvut.bank.adapters.DocumentAdapter
import com.cvut.bank.model.DocumentReference
import com.cvut.bank.service.DocumentService

class DocumentsActivity : AppCompatActivity() {

    private lateinit var documentsList: List<DocumentReference>
    private lateinit var documentsAdapter: DocumentAdapter
    private lateinit var recyclerView: RecyclerView
    private lateinit var addDocumentButton: Button


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_documents)

        supportActionBar?.hide()

        recyclerView = findViewById(R.id.recycler_view)
        recyclerView.layoutManager = LinearLayoutManager(this)

        val userId = intent.getStringExtra("userId")
        if (userId != null) {
            loadUserDocuments(userId)
        }
        addDocumentButton = findViewById(R.id.add_document_button)
        addDocumentButton.setOnClickListener {
            openFilePicker()
        }
        val backButton: ImageButton = findViewById(R.id.back_button)
        backButton.setOnClickListener {
            finish()
        }
    }

    private val documentService = DocumentService(this)

    private fun loadUserDocuments(userId: String) {
        documentService.loadUserDocuments(userId,
            onSuccess = { documents ->
                documentsList = documents
                documentsAdapter = DocumentAdapter(documentsList)
                recyclerView.adapter = documentsAdapter
            },
            onFailure = { exception ->
                exception.printStackTrace()
                Toast.makeText(this, "Načtení souborů se nepovedlo", Toast.LENGTH_SHORT).show()
            }
        )
    }

    private fun openFilePicker() {
        val intent = Intent(Intent.ACTION_GET_CONTENT)
        intent.type = "*/*"
        startActivityForResult(intent, PICK_FILE_REQUEST_CODE)
    }

    @Deprecated("Deprecated in Java")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == PICK_FILE_REQUEST_CODE && resultCode == RESULT_OK && data != null) {
            val fileUri = data.data
            if (fileUri != null) {
                uploadFileToFirebaseStorage(fileUri)
            }
        }
    }
    private fun uploadFileToFirebaseStorage(fileUri: Uri) {
        val userId = intent.getStringExtra("userId")
        if (userId != null) {
            documentService.uploadFileToFirebaseStorage(userId, fileUri,
                onSuccess = {
                    Toast.makeText(this, "Soubor byl úspěšně nahrán", Toast.LENGTH_SHORT).show()
                },
                onFailure = { exception ->
                    exception.printStackTrace()
                    Toast.makeText(this, "Nahrání souboru se nepovedlo", Toast.LENGTH_SHORT).show()
                }
            )
        }
        recreate()
    }
    companion object {
        private const val PICK_FILE_REQUEST_CODE = 1
    }
}