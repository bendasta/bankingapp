package com.cvut.bank.adapters

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.cvut.bank.R
import com.cvut.bank.model.Account
import com.cvut.bank.model.FinancialProduct
import com.cvut.bank.model.Investment
import com.cvut.bank.model.Loan
import com.cvut.bank.model.User
import com.cvut.bank.viewModel.AccountDetailActivity
import com.cvut.bank.viewModel.WelcomePageActivity
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.FirebaseFirestore

class AccountsAdapter(private val accounts: List<FinancialProduct>, private val isPlusButtonClicked: Boolean, private val user: Task<DocumentSnapshot>) : RecyclerView.Adapter<AccountsAdapter.ViewHolder>() {

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val accountNumber: TextView = view.findViewById(R.id.account_number)
        val accountBalance: TextView = view.findViewById(R.id.account_balance)
        val bankName: TextView = view.findViewById(R.id.bank_name)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.account_item, parent, false)
        return ViewHolder(view)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val account = accounts[position]
        holder.accountNumber.text = account.accountNumber.toString()
        holder.bankName.text = account.bank_name.toString()

        if (account is Investment) {
            holder.accountBalance.text = account.investmentAmount.toString() + " Kč"
        }
        else if (account is Loan) {
            holder.accountBalance.text = account.loanAmount.toString() + " Kč"
        }
        else{
            holder.accountBalance.text = account.balance.toString() + " Kč"
        }

        val layoutParams = holder.itemView.layoutParams as RecyclerView.LayoutParams
        layoutParams.leftMargin = 75
        holder.itemView.layoutParams = layoutParams

        val plusButton: Button = holder.itemView.findViewById(R.id.plusButton)
        user.addOnSuccessListener { documentSnapshot ->
            val userObject = documentSnapshot.toObject(User::class.java)
            if (isPlusButtonClicked && userObject?.watchlistRefs?.any { it.id == account.accountNumber.toString() } == false) {
                val animIn = AnimationUtils.loadAnimation(plusButton.context, R.anim.scale_up)
                plusButton.startAnimation(animIn)
                plusButton.visibility = View.VISIBLE
                plusButton.setOnClickListener {
                    user.addOnSuccessListener {
                        val db = FirebaseFirestore.getInstance()
                        val accountRef = db.collection("accounts").document(account.accountNumber.toString())
                        userObject.watchlistRefs.add(accountRef)

                        db.collection("users").document(userObject.userID ?:"")
                            .update("watchlistRefs", userObject.watchlistRefs)
                            .addOnSuccessListener {
                                plusButton.visibility = View.GONE
                                Toast.makeText(plusButton.context, "Účet byl úspěšně přidán do sledovaných služeb", Toast.LENGTH_SHORT).show()
                                val intent = Intent(plusButton.context, WelcomePageActivity::class.java)
                                intent.putExtra("userId", userObject.userID)
                                plusButton.context.startActivity(intent)
                                (plusButton.context as Activity).finish()
                            }
                            .addOnFailureListener {
                                Toast.makeText(plusButton.context, "Účet nebylo možné přidat do sledovaných služeb", Toast.LENGTH_SHORT).show()
                            }
                    }
                }


            }
            else {
                val animOut = AnimationUtils.loadAnimation(plusButton.context, R.anim.scale_down)
                plusButton.startAnimation(animOut)
                plusButton.visibility = View.GONE
            }

        }
        holder.itemView.setOnClickListener {
            val context = it.context
            val intent = Intent(context, AccountDetailActivity::class.java)

            intent.putExtra("account_number", account.accountNumber.toString())
            context.startActivity(intent)
        }

    }

    override fun getItemCount() = accounts.size
}