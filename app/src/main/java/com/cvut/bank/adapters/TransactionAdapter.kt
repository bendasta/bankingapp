package com.cvut.bank.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.cvut.bank.R
import com.cvut.bank.model.Transaction

class TransactionAdapter(private val transactions: List<Transaction>) : RecyclerView.Adapter<TransactionAdapter.TransactionViewHolder>() {

    class TransactionViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val transactionAmount: TextView = view.findViewById(R.id.transaction_amount)
        val transactionDate: TextView = view.findViewById(R.id.transaction_date)
        val transactionType: TextView = view.findViewById(R.id.transaction_type)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TransactionViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.transaction_item, parent, false)
        return TransactionViewHolder(view)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: TransactionViewHolder, position: Int) {
        val transaction = transactions[position]
        holder.transactionAmount.text = transaction.amount.toString() + " Kč"
        holder.transactionDate.text = transaction.timestamp.toString()
        holder.transactionType.text = transaction.type.toString()
    }

    override fun getItemCount() = transactions.size
}