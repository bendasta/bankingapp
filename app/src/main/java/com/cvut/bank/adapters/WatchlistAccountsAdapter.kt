package com.cvut.bank.adapters

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.cvut.bank.R
import com.cvut.bank.model.FinancialProduct
import com.cvut.bank.model.Investment
import com.cvut.bank.model.Loan
import com.cvut.bank.model.User
import com.cvut.bank.viewModel.AccountDetailActivity
import com.cvut.bank.viewModel.WatchButtonStateProvider
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.FirebaseFirestore

class WatchlistAccountsAdapter(
    private val accounts: MutableList<FinancialProduct>,
    private val user: Task<DocumentSnapshot>,
    private val watchButtonStateProvider: WatchButtonStateProvider
) : RecyclerView.Adapter<WatchlistAccountsAdapter.ViewHolder>() {

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val accountNumber: TextView = view.findViewById(R.id.account_number)
        val accountBalance: TextView = view.findViewById(R.id.account_balance)
        val bankName: TextView = view.findViewById(R.id.bank_name)
        val deleteButton: Button = view.findViewById(R.id.delete_button)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.account_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val account = accounts[position]
        holder.accountNumber.text = account.accountNumber.toString()
        holder.bankName.text = account.bank_name.toString()

        if (account.accountType.toString() == "Investment") {
            holder.accountBalance.text = (account as Investment).investmentAmount.toString() + " Kč"
        }
        else if (account.accountType.toString() == "Loan") {
            holder.accountBalance.text = (account as Loan).loanAmount.toString() + " Kč"
        }
        else {
            holder.accountBalance.text = account.balance.toString() + " Kč"
        }

        val layoutParams = holder.itemView.layoutParams as RecyclerView.LayoutParams
        layoutParams.leftMargin = 75

        if (watchButtonStateProvider.isWatchButtonClicked()) {
            val animIn = AnimationUtils.loadAnimation(holder.deleteButton.context, R.anim.scale_up)
            holder.deleteButton.startAnimation(animIn)
            holder.deleteButton.visibility = View.VISIBLE
            holder.deleteButton.setOnClickListener {
                user.addOnSuccessListener { documentSnapshot ->
                    val userObject = documentSnapshot.toObject(User::class.java)

                    val accountToRemove = accounts[position]
                    val db = FirebaseFirestore.getInstance()
                    val accountRefToRemove = db.collection("accounts").document(accountToRemove.accountNumber.toString())

                    userObject?.watchlistRefs?.remove(accountRefToRemove)
                    accounts.removeAt(position)
                    notifyItemRemoved(position)

                    db.collection("users").document(userObject?.userID?:"")
                        .update("watchlistRefs", userObject?.watchlistRefs)
                }
            }
        }
        else {
            val animOut = AnimationUtils.loadAnimation(holder.deleteButton.context, R.anim.scale_down)
            holder.deleteButton.startAnimation(animOut)
            holder.deleteButton.visibility = View.GONE
        }
        holder.itemView.setOnClickListener {
            val context = it.context
            val intent = Intent(context, AccountDetailActivity::class.java)

            intent.putExtra("account_number", account.accountNumber.toString())
            context.startActivity(intent)
        }

    }

    override fun getItemCount() = accounts.size
}