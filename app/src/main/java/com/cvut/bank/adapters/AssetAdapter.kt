package com.cvut.bank.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.cvut.bank.R
import com.cvut.bank.model.Asset

class AssetAdapter(private val assets: List<Asset>) : RecyclerView.Adapter<AssetAdapter.AssetViewHolder>() {

    class AssetViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val assetSymbol: TextView = view.findViewById(R.id.asset_symbol)
        val assetQuantity: TextView = view.findViewById(R.id.asset_quantity)
        val assetCurrentPrice: TextView = view.findViewById(R.id.asset_current_price)
        val assetDividend: TextView = view.findViewById(R.id.asset_dividend)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AssetViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.asset_item, parent, false)
        return AssetViewHolder(view)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: AssetViewHolder, position: Int) {
        val asset = assets[position]
        holder.assetSymbol.text = asset.symbol
        holder.assetQuantity.text = asset.quantity.toString()
        holder.assetCurrentPrice.text = asset.currentPrice.toString() + " Kč"

        if (asset.dividend != null) {
            holder.assetDividend.text =  " Dividenda: " + asset.dividend.toString() + "%"
        }
    }

    override fun getItemCount() = assets.size
}