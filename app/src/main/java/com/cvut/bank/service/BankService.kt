package com.cvut.bank.service

import com.cvut.bank.model.Account
import com.cvut.bank.model.FinancialProduct

interface BankService {
    fun getUserAccounts(userId: String): List<Account>
    fun getAccount(userId: String, accountNumber: String, bank: String): FinancialProduct?
}