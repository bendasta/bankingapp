package com.cvut.bank.service

import android.content.Context
import com.cvut.bank.bankClients.CSOBClient
import com.cvut.bank.model.Account
import com.cvut.bank.model.FinancialProduct

/**
 * Třída CSOBService slouží jako most mezi klientem a modelem.
 * Interaguje s CSOBClientem pro získání uživatelských účtů a detailů účtů.
 *
 * @constructor Vytváří instanci třídy CSOBService se specifikovaným kontextem.
 * @param context Kontext použitý k inicializaci CSOBClienta.
 */
class CSOBServiceImpl(context: Context) : BankService {
    private var client = CSOBClient(context)

    /**
     * Získá seznam účtů pro specifikovaného uživatele.
     *
     * @param userId ID uživatele, jehož účty mají být získány.
     * @return Seznam objektů [Account] spojených s uživatelem.
     */
    override fun getUserAccounts(userId: String): List<Account> {
        return client.getUserAccounts(userId)
    }

    /**
     * Získá detail specifikovaného účtu pro specifikovaného uživatele.
     *
     * @param userId ID uživatele, jehož účet má být získán.
     * @param accountNumber Číslo účtu, který má být získán.
     * @param bank Banka, ve které je účet veden.
     * @return Objekt [Account] pokud účet existuje, jinak null.
     */
    override fun getAccount(userId: String, accountNumber: String, bank: String): FinancialProduct? {
        return client.getAccount(userId, accountNumber, bank)
    }

    fun setClient(client: CSOBClient) {
        this.client = client
    }
}
