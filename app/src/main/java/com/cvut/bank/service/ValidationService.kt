package com.cvut.bank.service

/**
 * Třída ValidationService poskytuje metody pro ověření různých typů dat.
 */
class ValidationService {
    /**
     * Ověřuje, zda je řetězec neprázdný.
     *
     * @param input Vstupní řetězec k ověření.
     * @return true, pokud je řetězec neprázdný, jinak false.
     */
    fun isNotEmpty(input: String?): Boolean {
        return input != null && !input.isEmpty()
    }

    /**
     * Ověřuje, zda je zadaný řetězec platnou e-mailovou adresou.
     *
     * @param email E-mailová adresa k ověření.
     * @return true, pokud je e-mailová adresa platná, jinak false.
     */
    fun isValidEmail(email: String): Boolean {
        val validEmailFormat = "^[A-Za-z](.*)([@]{1})(.{1,})(\\.)(.{1,})"
        return email.matches(validEmailFormat.toRegex())
    }

    /**
     * Ověřuje, zda je zadané heslo platné.
     *
     * @param password Heslo k ověření.
     * @return true, pokud je heslo platné, jinak false.
     */
    fun isValidPassword(password: String): Boolean {
        val validPasswordFormat =
            "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=._])(?=\\S+$).{8,}$"
        return password.matches(validPasswordFormat.toRegex())
    }

    /**
     * Ověřuje, zda je zadané číslo účtu platné.
     *
     * @param accountNumber Číslo účtu k ověření.
     * @return true, pokud je číslo účtu platné, jinak false.
     */
    fun isValidAccountNumber(accountNumber: String): Boolean {
        val validAccountNumberFormat = "\\d{10}"
        return accountNumber.matches(validAccountNumberFormat.toRegex())
    }

    /**
     * Ověřuje, zda je zadaný poskytovatel služby platný.
     *
     * @param serviceProvider      Poskytovatel služby k ověření.
     * @param validServiceProviders Pole platných poskytovatelů služeb.
     * @return true, pokud je poskytovatel služby platný, jinak false.
     */
    fun isValidServiceProvider(
        serviceProvider: String,
        validServiceProviders: Array<String>
    ): Boolean {
        for (provider in validServiceProviders) {
            if (provider == serviceProvider) {
                return true
            }
        }
        return false
    }
}