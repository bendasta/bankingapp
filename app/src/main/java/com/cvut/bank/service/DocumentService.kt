package com.cvut.bank.service

import android.content.Context
import android.net.Uri
import android.provider.OpenableColumns
import android.widget.Toast
import com.cvut.bank.model.DocumentReference
import com.cvut.bank.model.User
import com.google.firebase.Firebase
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.firestore
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.storage

class DocumentService(private val context: Context) {
    private var firestore: FirebaseFirestore = Firebase.firestore
    private var storage: FirebaseStorage = Firebase.storage

    fun loadUserDocuments(userId: String, onSuccess: (List<DocumentReference>) -> Unit, onFailure: (Exception) -> Unit) {
        firestore.collection("users").document(userId).get().addOnSuccessListener { documentSnapshot ->
            val user = documentSnapshot.toObject(User::class.java)
            val documentReferences = user?.documentReference
            val documentsList = mutableListOf<DocumentReference>()
            documentReferences?.forEach { documentReference ->
                val storageReference = storage.getReferenceFromUrl("gs://onlinebankingapp-41fe9.appspot.com/${documentReference.path}")
                storageReference.downloadUrl.addOnSuccessListener { downloadUrl ->
                    val name = storageReference.name
                    val document = DocumentReference(userId, name, downloadUrl.toString())
                    documentsList.add(document)
                    onSuccess(documentsList)
                }.addOnFailureListener { exception ->
                    onFailure(exception)
                }
            }
        }.addOnFailureListener { exception ->
            onFailure(exception)
        }
    }

    fun uploadFileToFirebaseStorage(userId: String, fileUri: Uri, onSuccess: (Uri) -> Unit, onFailure: (Exception) -> Unit) {
        val storageRef = storage.reference
        val fileName = getFileName(fileUri) ?: ""

        if (!fileName.endsWith(".pdf")) {
            Toast.makeText(context, "Je možné nahrát pouze soubory s příponou .pdf!", Toast.LENGTH_LONG).show()
            return
        }

        val documentRef = storageRef.child("documents/$fileName")

        val uploadTask = documentRef.putFile(fileUri)
        uploadTask.addOnSuccessListener {
            documentRef.downloadUrl.addOnSuccessListener { downloadUri ->
                val firestoreDocumentRef = firestore.document("documents/$fileName")

                firestore.collection("users").document(userId).get().addOnSuccessListener { documentSnapshot ->
                    val user = documentSnapshot.toObject(User::class.java)
                    user?.documentReference?.add(firestoreDocumentRef)

                    firestore.collection("users").document(userId).set(user!!)
                }
                onSuccess(downloadUri)
            }
        }.addOnFailureListener {exception ->
            exception.printStackTrace()
            onFailure(exception)
        }
    }

    private fun getFileName(uri: Uri): String? {
        var result: String? = null
        if (uri.scheme == "content") {
            val cursor = context.contentResolver.query(uri, null, null, null, null)
            cursor.use {
                if (cursor != null && cursor.moveToFirst()) {
                    val columnIndex = cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME)
                    if (columnIndex != -1) {
                        result = cursor.getString(columnIndex)
                    }
                }
            }
        }
        if (result == null) {
            result = uri.path
            val cut = result?.lastIndexOf('/')
            if (cut != null && cut != -1) {
                result = result?.substring(cut + 1)
            }
        }
        return result
    }

    fun setDocumentService(storage: FirebaseStorage) {
        this.storage = storage
    }
}