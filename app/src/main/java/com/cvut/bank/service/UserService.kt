package com.cvut.bank.service

import android.content.Context
import android.widget.Toast
import com.cvut.bank.model.Account
import com.cvut.bank.model.FinancialProduct
import com.cvut.bank.model.Investment
import com.cvut.bank.model.Loan
import com.cvut.bank.model.SavingsAccount
import com.cvut.bank.model.User
import com.google.android.gms.tasks.Task
import com.google.android.gms.tasks.Tasks
import com.google.firebase.Firebase
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.auth
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QuerySnapshot
import com.google.firebase.firestore.firestore
import java.util.concurrent.ExecutionException
import kotlin.reflect.KClass

/**
 * Třída UserService poskytuje metody pro manipulaci s uživatelskými účty a autentizací.
 *
 * Tato třída zajišťuje správu uživatelských účtů, vytváření nových uživatelů, autentizaci,
 * získávání informací o uživatelích a aktualizaci účtů pomocí bankovních API.
 *
 * @property context Kontext aplikace.
 * @property auth Instance Firebase autentikační služby pro správu uživatelských přihlašovacích údajů.
 * @property db Instance Firebase Firestore pro komunikaci s databází.
 * @property bankServices Mapa poskytovatelů bankovních služeb, kde klíče představují názvy bank a hodnoty jsou instance odpovídajících služeb.
 * @property accountTypes Mapa typů finančních produktů, kde klíče představují názvy typů a hodnoty jsou třídy odpovídajících finančních produktů.
 */
class UserService(private val context: Context) {

    private var auth: FirebaseAuth = Firebase.auth
    private var db: FirebaseFirestore = Firebase.firestore

    private val bankServices = mapOf(
        "KB" to KBServiceImpl(context),
        "ČSOB" to CSOBServiceImpl(context),
        "Coinbase" to CoinbaseServiceImpl(context),
        "George" to GeorgeServiceImpl(context)
    )

    private val accountTypes: Map<String, KClass<out FinancialProduct>> = mapOf(
        "Account" to Account::class,
        "Savings" to SavingsAccount::class,
        "Loan" to Loan::class,
        "Investment" to Investment::class
    )

    /**
     * Vytvoří nového uživatele s daným jménem, příjmením, e-mailovou adresou a heslem.
     *
     * @param name Jméno nového uživatele.
     * @param surname Příjmení nového uživatele.
     * @param email E-mailová adresa nového uživatele.
     * @param password Heslo nového uživatele.
     * @return [Task] reprezentující výsledek operace vytváření uživatele.
     */
    fun createUser(
        name: String,
        surname: String,
        email: String,
        password: String
    ): Task<AuthResult> {
        return auth.createUserWithEmailAndPassword(email, password).addOnCompleteListener { task ->
            if (task.isSuccessful) {
                val uid = FirebaseAuth.getInstance().currentUser?.uid
                if (uid != null) {
                    val user = User(name, surname, email, uid)
                    db.collection("users").document(uid).set(user)
                }
            } else {
                Toast.makeText(context, "Nepodařilo se vytvořit uživatele", Toast.LENGTH_SHORT)
                    .show()
            }

        }
    }

    /**
     * Přihlásí uživatele s danou e-mailovou adresou a heslem.
     *
     * @param email E-mailová adresa uživatele pro přihlášení.
     * @param password Heslo uživatele pro přihlášení.
     * @return [Task] reprezentující výsledek operace přihlášení.
     */
    fun loginUser(email: String, password: String): Task<AuthResult> {
        return auth.signInWithEmailAndPassword(email, password)
    }

    /**
     * Vrací ID aktuálně autentizovaného uživatele.
     *
     * @return ID aktuálně autentizovaného uživatele.
     */
    fun getAuthenticatedUserId(): String? {
        return FirebaseAuth.getInstance().currentUser?.uid
    }

    /**
     * Vrací informace o uživateli s daným ID.
     *
     * @param userId ID uživatele.
     * @return [Task] reprezentující výsledek operace získání informací o uživateli.
     */
    fun getUser(userId: String): Task<DocumentSnapshot> {
        try {
            return db.collection("users").document(userId).get()
        } catch (exception: ExecutionException) {
            exception.printStackTrace();
        }
        return Tasks.forException(IllegalStateException("Uživatel nenalezen"))
    }

    /**
     * Vrací účty uživatele.
     *
     * @return [Task] reprezentující výsledek operace získání účtů uživatele.
     */
    fun getUserAccounts(): Task<QuerySnapshot> {
        val userId = getAuthenticatedUserId()
        return if (userId != null) {
            db.collection("accounts").whereEqualTo("ownerId", userId).get()
        } else {
            Tasks.forException(IllegalStateException("Přihlášený uživatel nenalezen"))
        }
    }

    /**
     * Aktualizuje informace o uživateli.
     *
     * @param user Objekt reprezentující uživatele k aktualizaci.
     * @return [Task] reprezentující výsledek operace aktualizace uživatele.
     */
    fun updateUser(user: User?): Task<Void> {
        return if (user != null) {
            db.collection("users").document(user.userID!!).set(user)
        } else {
            Tasks.forException(IllegalArgumentException("Uživatel nebyl nalezen"))
        }
    }

    /**
     * Aktualizuje účty uživatele pomocí bankovních API.
     *
     * Tato metoda aktualizuje informace o účtech uživatele získané z externích bankovních API.
     * Pro každý účet uživatele získá aktuální informace o stavu z odpovídající bankovní služby
     * a aktualizuje údaje v aplikaci.
     *
     * @param userId ID uživatele, jehož účty se mají aktualizovat.
     */
    fun updateUserAccountsFromBankApi(userId: String) {
        val userRef = db.collection("users").document(userId)
        userRef.get().addOnSuccessListener { documentSnapshot ->
            val user = documentSnapshot.toObject(User::class.java)
            if (user != null) {
                user.financialProductsRefs.forEach { financialProductRef ->
                    var specificFinancialProduct: FinancialProduct? = null
                    financialProductRef.get().addOnSuccessListener { documentSnapshot ->
                        val accountType = documentSnapshot.getString("accountType")
                        val accountTypeClass = accountTypes[accountType]
                        if (accountTypeClass != null) {
                            specificFinancialProduct =
                                documentSnapshot.toObject(accountTypeClass.java)
                            if (specificFinancialProduct is FinancialProduct) {
                                user.financialProducts.add(specificFinancialProduct!!)
                            }
                        }
                        val bankService = bankServices[specificFinancialProduct?.bank_name]
                        val bankAccount = bankService?.getAccount(
                            userId,
                            specificFinancialProduct?.accountNumber.toString(),
                            specificFinancialProduct?.bank_name.toString()
                        )
                        if (bankAccount != null) {
                            specificFinancialProduct?.balance = bankAccount.balance
                            specificFinancialProduct?.accountNumber = bankAccount.accountNumber
                            specificFinancialProduct?.bank_name = bankAccount.bank_name
                            specificFinancialProduct?.ownerId = bankAccount.ownerId
                            specificFinancialProduct?.accountType = bankAccount.accountType
                            when (specificFinancialProduct) {
                                is Account -> {
                                    if (specificFinancialProduct is SavingsAccount) {
                                        (specificFinancialProduct as SavingsAccount).interestRate =
                                            (bankAccount as SavingsAccount).interestRate
                                        (specificFinancialProduct as SavingsAccount).currency =
                                            (bankAccount as Account).currency
                                    }
                                    else if (specificFinancialProduct is Investment) {
                                        (specificFinancialProduct as Investment).investmentAmount = (bankAccount as Investment).investmentAmount
                                        (specificFinancialProduct as Investment).assets = bankAccount.assets
                                    }
                                    else{
                                        (specificFinancialProduct as Account).transactions = (bankAccount as Account).transactions
                                        (specificFinancialProduct as Account).currency = bankAccount.currency
                                    }
                                }
                                is Loan -> {
                                    (specificFinancialProduct as Loan).loanAmount = (bankAccount as Loan).loanAmount
                                    (specificFinancialProduct as Loan).monthlyPayment = bankAccount.monthlyPayment
                                    (specificFinancialProduct as Loan).interestRate = bankAccount.interestRate
                                    (specificFinancialProduct as Loan).remainingMonths = bankAccount.remainingMonths
                                }
                            }
                            financialProductRef.set(specificFinancialProduct!!)
                        }
                    }
                }
            } else {
                Toast.makeText(context, "Uživatel nebyl nalezen", Toast.LENGTH_SHORT).show()
            }
        }.addOnFailureListener {
            Toast.makeText(context, "Nebylo možné získat informace o uživateli", Toast.LENGTH_SHORT).show()
        }
    }

    /**
     * Přidá nový účet uživateli.
     *
     * @param userId ID uživatele.
     * @param accountNumber Číslo účtu.
     * @param serviceProvider Název poskytovatele služby, který spravuje účet.
     * @return [Task] reprezentující výsledek operace přidání účtu.
     */
    fun addAccount(userId: String, accountNumber: String, serviceProvider: String): Task<Void> {
        val bankService = getBankService(serviceProvider)
        return if (bankService == null) {
            Tasks.forException<Void>(IllegalArgumentException("Tato služba není dostupná"))
        } else {
            val account = bankService.getAccount(userId, accountNumber, serviceProvider)
            if (account != null) {
                val saveTask = saveAccountToDatabase(account)
                val addUserTask = addUserAccountReference(userId, account)
                Tasks.whenAllSuccess<Void>(saveTask, addUserTask).continueWithTask { task ->
                    if (task.isSuccessful) {
                        Tasks.forResult(null)
                    } else {
                        Tasks.forException(task.exception!!)
                    }
                }
            } else {
                Tasks.forException(IllegalArgumentException("Účet nebyl nalezen"))
            }
        }
    }

    /**
     * Vrací bankovní službu pro daného poskytovatele.
     *
     * @param serviceProvider Název poskytovatele bankovní služby.
     * @return Instance bankovní služby, nebo null pokud poskytovatel není dostupný.
     */
    private fun getBankService(serviceProvider: String): BankService? {
        return when (serviceProvider) {
            "Coinbase" -> CoinbaseServiceImpl(context)
            "KB" -> KBServiceImpl(context)
            "George" -> GeorgeServiceImpl(context)
            "ČSOB" -> CSOBServiceImpl(context)
            else -> null
        }
    }

    /**
     * Uloží účet do databáze.
     *
     * @param account Objekt reprezentující účet, který se má uložit.
     * @return [Task] reprezentující výsledek operace uložení účtu do databáze.
     */
    private fun saveAccountToDatabase(account: FinancialProduct?): Task<Void> {
        val db = FirebaseFirestore.getInstance()
        val accountDocument = db.collection("accounts").document(account?.accountNumber.toString())
        return accountDocument.get().continueWithTask { task ->
            if (task.isSuccessful && !task.result!!.exists()) {
                val accountMap = account?.getAccountMap()
                if (accountMap != null) {
                    accountDocument.set(accountMap).continueWithTask { setTask ->
                        if (setTask.isSuccessful) {
                            Tasks.forResult(null)
                        } else {
                            Tasks.forException(setTask.exception!!)
                        }
                    }
                } else {
                    Tasks.forException(IllegalArgumentException("Account map is null"))
                }
            } else {
                Tasks.forResult(null)
            }
        }
    }

    /**
     * Přidá odkaz na účet do uživatelského záznamu.
     *
     * @param userId ID uživatele, ke kterému se má odkaz přidat.
     * @param account Objekt reprezentující účet, který se má přidat.
     * @return [Task] reprezentující výsledek operace přidání odkazu na účet do uživatelského záznamu.
     */
    private fun addUserAccountReference(userId: String, account: FinancialProduct?): Task<Void> {
        val db = FirebaseFirestore.getInstance()
        val accountReference = db.collection("accounts").document(account?.accountNumber.toString())
        return db.collection("users").document(userId)
            .update("financialProductsRefs", FieldValue.arrayUnion(accountReference))
    }

    /**
     * Nastaví instanci autentikační služby Firebase.
     *
     * @param auth Instance autentikační služby Firebase k nastavení.
     */
    fun setAuth(auth: FirebaseAuth) {
        this.auth = auth
    }

    /**
     * Nastaví instanci databázové služby Firestore.
     *
     * @param db Instance databázové služby Firestore k nastavení.
     */
    fun setDb(db: FirebaseFirestore) {
        this.db = db
    }
}