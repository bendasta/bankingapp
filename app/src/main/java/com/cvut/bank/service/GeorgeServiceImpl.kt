package com.cvut.bank.service

import android.content.Context
import com.cvut.bank.bankClients.GeorgeClient
import com.cvut.bank.model.Account
import com.cvut.bank.model.FinancialProduct

/**
 * Třída GeorgeService slouží jako most mezi klientem a modelem.
 * Interaguje s GeorgeClientem pro získání uživatelských účtů a detailů účtů.
 *
 * @constructor Vytváří instanci třídy GeorgeService se specifikovaným kontextem.
 * @param context Kontext použitý k inicializaci GeorgeClienta.
 */
class GeorgeServiceImpl(context: Context) : BankService {
    private var client = GeorgeClient(context)

    /**
     * Získá seznam účtů pro specifikovaného uživatele.
     *
     * @param userId ID uživatele, jehož účty mají být získány.
     * @return Seznam objektů [Account] spojených s uživatelem.
     */
    override fun getUserAccounts(userId: String): List<Account> {
        return client.getUserAccounts(userId)
    }

    /**
     * Získá detail specifikovaného účtu pro specifikovaného uživatele.
     *
     * @param userId ID uživatele, jehož účet má být získán.
     * @param accountNumber Číslo účtu, který má být získán.
     * @param bank Banka, ve které je účet veden.
     * @return Objekt [Account] pokud účet existuje, jinak null.
     */
    override fun getAccount(userId: String, accountNumber: String, bank: String): FinancialProduct? {
        return client.getAccount(userId, accountNumber, bank)
    }

    fun setClient(client: GeorgeClient) {
        this.client = client
    }
}
