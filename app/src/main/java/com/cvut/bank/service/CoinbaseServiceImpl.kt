package com.cvut.bank.service

import android.content.Context
import com.cvut.bank.bankClients.CoinbaseClient
import com.cvut.bank.model.Account
import com.cvut.bank.model.FinancialProduct

/**
 * Třída CoinbaseService slouží jako most mezi klientem a modelem.
 * Interaguje s CoinbaseClientem pro získání uživatelských účtů a detailů účtů.
 *
 * @constructor Vytváří instanci třídy CoinbaseService se specifikovaným kontextem.
 * @param context Kontext použitý k inicializaci CoinbaseClienta.
 */
class CoinbaseServiceImpl(context: Context) : BankService {
    private var client = CoinbaseClient(context)

    /**
     * Získá seznam účtů pro specifikovaného uživatele.
     *
     * @param userId ID uživatele, jehož účty mají být získány.
     * @return Seznam objektů [Account] spojených s uživatelem.
     */
    override fun getUserAccounts(userId: String): List<Account> {
        return client.getUserAccounts(userId)
    }

    /**
     * Získá detail specifikovaného účtu pro specifikovaného uživatele.
     *
     * @param userId ID uživatele, jehož účet má být získán.
     * @param accountNumber Číslo účtu, který má být získán.
     * @param bank Banka, ve které je účet veden.
     * @return Objekt [Account] pokud účet existuje, jinak null.
     */
    override fun getAccount(userId: String, accountNumber: String, bank: String): FinancialProduct? {
        return client.getAccount(userId, accountNumber, bank)
    }

    fun setClient(client: CoinbaseClient) {
        this.client = client
    }
}
