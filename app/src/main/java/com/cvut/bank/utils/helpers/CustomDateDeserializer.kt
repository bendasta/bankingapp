package com.cvut.bank.utils.helpers

import android.annotation.SuppressLint
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import java.text.SimpleDateFormat
import java.util.*

class CustomDateDeserializer : JsonDeserializer<Date>() {
    @SuppressLint("SimpleDateFormat")
    private val dateFormat = SimpleDateFormat("HH:mm:ss")

    override fun deserialize(p: JsonParser, ctxt: DeserializationContext): Date? {
        val dateAsString = p.text.trim()
        return dateFormat.parse(dateAsString)
    }
}