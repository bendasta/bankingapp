package com.cvut.bank.utils.enums

enum class DocumentType {
    ID_CARD,
    PROOF_OF_ADDRESS,
    INCOME_STATEMENT
}