package com.cvut.bank.utils.enums

enum class ProductType {
    Account,
    Savings,
    Loan,
    Investment
}