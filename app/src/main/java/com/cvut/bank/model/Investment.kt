package com.cvut.bank.model

import com.fasterxml.jackson.annotation.JsonProperty
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.OneToMany

@Entity
open class Investment : Account() {
    @JsonProperty("investment_amount")
    @Column(name = "investment_amount", precision = 19, scale = 2)
    open var investmentAmount: Double? = null

    @JsonProperty("assets")
    @OneToMany(mappedBy = "investment", orphanRemoval = true)
    open var assets: MutableList<Asset> = mutableListOf()



    override fun equals(other: Any?): Boolean {
        if (!super.equals(other)) return false
        if (javaClass != other.javaClass) return false

        other as Investment

        return investmentAmount == other.investmentAmount
    }

    override fun hashCode(): Int {
        var result = super.hashCode()
        result = 31 * result + (investmentAmount?.hashCode() ?: 0)
        return result
    }

    override fun getAccountMap(): Map<String, Any?> {
        return mapOf(
            "ownerId" to ownerId,
            "accountType" to accountType.toString(),
            "accountNumber" to accountNumber,
            "balance" to balance,
            "transactions" to transactions,
            "bank_name" to bank_name,
            "investment_amount" to investmentAmount,
            "assets" to assets.map { it.getAssetMap() }
        )
    }
}