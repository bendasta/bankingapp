package com.cvut.bank.model

import com.cvut.bank.utils.enums.ProductType
import com.fasterxml.jackson.annotation.JsonProperty
import java.io.Serializable
import java.util.UUID
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Enumerated
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.Table

@Entity
@Table(name = "financial_product")
abstract class FinancialProduct : Serializable {
    abstract var balance: Double?

    @JsonProperty("id")
    @Id
    @Column(name = "id", nullable = false)
    open var id: UUID? = null

    @JsonProperty("account_type")
    @Enumerated
    @Column(name = "account_type")
    open var accountType: ProductType? = null

    @JsonProperty("account_number")
    @Column(name = "account_number")
    open var accountNumber: Long? = null

    @JsonProperty("owner_id")
    @ManyToOne
    @JoinColumn(name = "owner_id")
    open var ownerId: String? = null

    abstract val transactions: List<Transaction>


    @JsonProperty("bank_name")
    @Column(name = "bank_name")
    open var bank_name: String? = null
    abstract fun getAccountMap(): Map<String, Any?>
}