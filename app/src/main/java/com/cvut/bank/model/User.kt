package com.cvut.bank.model

import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.Exclude
import java.io.Serializable
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.OneToMany
import javax.persistence.Table

@Entity
@Table(name = "user")
open class User(
    var name: String? = null, val surname: String? = null, val email: String? = null, private val uid: String? = null,
    val documentReference: MutableList<DocumentReference>? = null,
    var profileImageUrl: String? = null) : Serializable {

    @OneToMany(mappedBy = "user", orphanRemoval = true)
    open var transactions: MutableList<Transaction> = mutableListOf()

    @Transient
    open var financialProductsRefs: MutableList<DocumentReference> = mutableListOf()

    @Id
    @Column(name = "user_id", nullable = false)
    open var userID: String? = uid

    @OneToMany(mappedBy = "ownerId", orphanRemoval = true)
    open var financialProducts: MutableList<FinancialProduct> = mutableListOf()

    val watchlistRefs: MutableList<DocumentReference> = mutableListOf()

    open var watchlist: MutableList<FinancialProduct> = mutableListOf()
}