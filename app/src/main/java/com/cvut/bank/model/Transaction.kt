package com.cvut.bank.model

import com.fasterxml.jackson.annotation.JsonProperty
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne

@Entity
open class Transaction {
    @JsonProperty("transaction_id")
    @Id
    @Column(name = "transaction_id")
    open var transactionId: Int? = null

    @JsonProperty("type")
    @Column(name = "type")
    open var type: String? = null

    @JsonProperty("amount")
    @Column(name = "amount")
    open var amount: Long? = null

    @JsonProperty("timestamp")
    @Column(name = "timestamp")
    open var timestamp: String? = null

    @ManyToOne
    @JoinColumn(name = "account_id")
    open var account: Account? = null

    @ManyToOne
    @JoinColumn(name = "user_user_id")
    open var user: User? = null
}