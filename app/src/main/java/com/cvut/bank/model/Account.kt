package com.cvut.bank.model

import com.fasterxml.jackson.annotation.JsonProperty
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.OneToMany

@Entity
open class Account: FinancialProduct() {
    @JsonProperty("balance")
    @Column(name = "balance")
    override var balance: Double? = null

    @JsonProperty("currency")
    @Column(name = "currency")
    open var currency: String? = null

    @JsonProperty("transactions")
    @OneToMany(mappedBy = "account", orphanRemoval = true)
    override var transactions: MutableList<Transaction> = mutableListOf()

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Account

        if (accountNumber != other.accountNumber) return false
        if (balance != other.balance) return false

        return true
    }

    override fun hashCode(): Int {
        var result = accountNumber?.hashCode() ?: 0
        result = 31 * result + (balance?.hashCode() ?: 0)
        return result
    }

    override fun getAccountMap(): Map<String, Any?> {
        return mapOf(
            "ownerId" to ownerId,
            "accountType" to accountType.toString(),
            "accountNumber" to accountNumber,
            "balance" to balance,
            "transactions" to transactions,
            "bank_name" to bank_name
        )
    }
}