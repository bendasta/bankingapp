package com.cvut.bank.model

import com.cvut.bank.utils.helpers.CustomDateDeserializer
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import java.util.Date
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Temporal
import javax.persistence.TemporalType

@Entity
open class Loan(override var balance: Double? = null) : FinancialProduct(){
    @JsonProperty("until")
    @Temporal(TemporalType.DATE)
    @Column(name = "until")
    open var until: Date? = null

    @JsonProperty("loan_amount")
    @Column(name = "loan_amount")
    open var loanAmount: Double? = null

    @JsonProperty("currency")
    @Column(name = "currency")
    open var currency: String? = null

    @JsonProperty("monthly_payment")
    @Column(name = "monthly_payment", precision = 19, scale = 2)
    open var monthlyPayment: Double? = null

    @JsonProperty("interest_rate")
    @Column(name = "interest_rate")
    open var interestRate: Float? = null

    @JsonProperty("remaining_months")
    @Column(name = "remaining_months")
    @JsonDeserialize(using = CustomDateDeserializer::class)
    open var remainingMonths: Date? = null

    @JsonProperty("transactions")
    override var transactions: MutableList<Transaction> = mutableListOf()

    override fun equals(other: Any?): Boolean {
        if (!super.equals(other)) return false
        if (javaClass != other.javaClass) return false

        other as Loan

        if (until != other.until) return false
        if (loanAmount != other.loanAmount) return false
        if (monthlyPayment != other.monthlyPayment) return false
        if (interestRate != other.interestRate) return false
        if (remainingMonths != other.remainingMonths) return false

        return true
    }

    override fun hashCode(): Int {
        var result = super.hashCode()
        result = 31 * result + (until?.hashCode() ?: 0)
        result = 31 * result + (loanAmount?.hashCode() ?: 0)
        result = 31 * result + (monthlyPayment?.hashCode() ?: 0)
        result = 31 * result + (interestRate?.hashCode() ?: 0)
        result = 31 * result + (remainingMonths?.hashCode() ?: 0)
        return result
    }
    override fun getAccountMap(): Map<String, Any?> {
        return mapOf(
            "ownerId" to ownerId,
            "accountType" to accountType.toString(),
            "accountNumber" to accountNumber,
            "bank_name" to bank_name,
            "until" to until,
            "loan_amount" to loanAmount,
            "monthly_payment" to monthlyPayment,
            "interest_rate" to interestRate,
            "remaining_months" to remainingMonths,
            "transactions" to transactions
        )
    }
}