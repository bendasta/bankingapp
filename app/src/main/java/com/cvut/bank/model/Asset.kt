package com.cvut.bank.model

import com.fasterxml.jackson.annotation.JsonProperty
import java.util.UUID
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.Table

@Entity
@Table(name = "asset")
open class Asset {
    @Id
    @Column(name = "id", nullable = false)
    open var id: UUID? = null

    @JsonProperty("symbol")
    @Column(name = "symbol")
    open var symbol: String? = null

    @JsonProperty("purchase_price")
    @Column(name = "purchase_price")
    open var purchasePrice: Double? = null

    @JsonProperty("current_price")
    @Column(name = "current_price")
    open var currentPrice: Double? = null

    @JsonProperty("quantity")
    @Column(name = "quantity")
    open var quantity: Int? = null

    @JsonProperty("dividend")
    @Column(name = "dividend")
    open var dividend: Double? = null

    @JsonProperty("currency")
    @Column(name = "currency")
    open var currency: String? = null

    @ManyToOne
    @JoinColumn(name = "investment_id")
    open var investment: Investment? = null

    fun getAssetMap(): Map<String, Any?> {
        return mapOf(
            "symbol" to symbol,
            "purchase_price" to purchasePrice,
            "current_price" to currentPrice,
            "quantity" to quantity,
            "dividend" to dividend,
            "currency" to currency
        )
    }
}