package com.cvut.bank.model

import com.cvut.bank.utils.enums.DocumentType
import java.util.UUID
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Enumerated
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "document_reference")
open class DocumentReference(val user: String? = null, val name: String? = null, val downloadUrl: String? = null)
{
    @Id
    @Column(name = "id", nullable = false)
    open var id: UUID? = null

}