package com.cvut.bank.model

import com.fasterxml.jackson.annotation.JsonProperty
import javax.persistence.Column
import javax.persistence.Entity

@Entity
open class SavingsAccount: Account() {
    @JsonProperty("interest_rate")
    @Column(name = "interest_rate")
    open var interestRate: Float? = null

    override fun equals(other: Any?): Boolean {
        if (!super.equals(other)) return false
        if (javaClass != other.javaClass) return false

        other as SavingsAccount

        return interestRate == other.interestRate
    }

    override fun hashCode(): Int {
        var result = super.hashCode()
        result = 31 * result + (interestRate?.hashCode() ?: 0)
        return result
    }

    override fun getAccountMap(): Map<String, Any?> {
        return mapOf(
            "ownerId" to ownerId,
            "accountType" to accountType.toString(),
            "accountNumber" to accountNumber,
            "balance" to balance,
            "transactions" to transactions,
            "bank_name" to bank_name,
            "interest_rate" to interestRate
        )
    }
}