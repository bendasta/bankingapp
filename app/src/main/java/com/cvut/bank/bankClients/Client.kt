package com.cvut.bank.bankClients

import com.cvut.bank.model.Account
import com.cvut.bank.model.FinancialProduct

interface Client {
    fun getUserAccounts(userId: String): List<Account>
    fun getAccount(userId: String, accountNumber: String, bank: String): FinancialProduct?
}