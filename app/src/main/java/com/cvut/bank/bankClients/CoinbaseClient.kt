package com.cvut.bank.bankClients

import android.content.Context
import com.cvut.bank.model.Account
import com.cvut.bank.model.FinancialProduct
import com.cvut.bank.model.Investment
import com.cvut.bank.model.Loan
import com.cvut.bank.model.SavingsAccount
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import java.io.File
import java.io.IOException
import kotlin.reflect.KClass

open class CoinbaseClient(private val context: Context): Client{
    private val accountTypes: Map<String, KClass<out FinancialProduct>> = mapOf(
        "Account" to Account::class,
        "Savings" to SavingsAccount::class,
        "Loan" to Loan::class,
        "Investment" to Investment::class
    )

    override fun getUserAccounts(userId: String): List<Account> {
        val jsonFile = File("BanksAPIs/Coinbase/Users/$userId.json")
        val json = jsonFile.readText()

        val mapper = jacksonObjectMapper()
        val userData: Map<String, Any> = mapper.readValue(json)
        val accountDataList: List<Map<String, Any>> = userData["accounts"] as List<Map<String, Any>>

        val accounts = mutableListOf<Account>()
        for (data in accountDataList) {
            val accountDescription = data["account_type"] as String
            val accountClass = accountTypes[accountDescription]
            val account = accountClass?.java?.getDeclaredConstructor()?.newInstance() as Account?
            if (account != null) {
                accounts.add(account)
            }
        }

        return accounts
    }
    override fun getAccount(userId: String, accountNumber: String, bank: String): FinancialProduct? {
        try {
            val assetManager = context.assets
            val inputStream = assetManager.open("BanksAPIs/$bank/Accounts/$accountNumber.json")
            val json = inputStream.bufferedReader().use { it.readText() }

            val mapper = jacksonObjectMapper()
            val accountData: Map<String, Any> = mapper.readValue(json)

            if (accountData["owner_id"] == userId) {
                val accountType = accountData["account_type"] as String
                val accountClass = accountTypes[accountType]
                val account = if (accountClass?.java?.let { Account::class.java.isAssignableFrom(it) } == true) {
                    accountClass.java.getDeclaredConstructor()?.newInstance() as Account?
                } else {
                    accountClass?.java?.getDeclaredConstructor()?.newInstance() as Loan?
                }
                if (account != null) {
                    mapper.readerForUpdating(account).readValue<Map<String, Any>>(json)
                    account.bank_name = bank
                    return account
                }
            }
        } catch (e: IOException) {
            e.printStackTrace()
        }

        return null
    }

}