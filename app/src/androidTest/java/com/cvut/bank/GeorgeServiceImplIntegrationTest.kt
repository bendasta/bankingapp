package com.cvut.bank

import androidx.test.platform.app.InstrumentationRegistry
import com.cvut.bank.bankClients.GeorgeClient
import com.cvut.bank.model.Account
import com.cvut.bank.model.FinancialProduct
import com.cvut.bank.model.Investment
import com.cvut.bank.model.Loan
import com.cvut.bank.model.SavingsAccount
import com.cvut.bank.service.GeorgeServiceImpl
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class GeorgeServiceImplIntegrationTest {
    @Mock
    private lateinit var georgeClient: GeorgeClient

    private val context = InstrumentationRegistry.getInstrumentation().context

    private lateinit var georgeServiceImpl: GeorgeServiceImpl

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        georgeServiceImpl = GeorgeServiceImpl(context)
        georgeServiceImpl.setClient(georgeClient)
    }

    @Test
    fun testGetUserAccounts() {
        val userId = "VItzY9wTcTXQ5ZI2HkfFhoam0eA2"
        val mapper = jacksonObjectMapper()
        val jsonFile = context.assets.open("BanksAPIs/George/Users/$userId.json")
        val userData: Map<String, Any> = mapper.readValue(jsonFile)
        val expectedAccounts: List<Account> = (userData["accounts"] as List<Account>)
        Mockito.`when`(georgeClient.getUserAccounts(userId)).thenReturn(expectedAccounts)

        val actualAccounts = georgeServiceImpl.getUserAccounts(userId)

        Assert.assertEquals(expectedAccounts, actualAccounts)
    }

    @Test
    fun testGetAccount() {
        val userId = "VItzY9wTcTXQ5ZI2HkfFhoam0eA2"
        val accountNumber = "1986443216"
        val bank = "George"
        val mapper = jacksonObjectMapper()
        val jsonFile = context.assets.open("BanksAPIs/$bank/Accounts/$accountNumber.json").bufferedReader().use { it.readText() }
        val accountData: Map<String, Any> = mapper.readValue(jsonFile)
        val accountType = accountData["account_type"] as String

        val expectedAccount: FinancialProduct? = when (accountType) {
            "Loan" -> mapper.readValue(jsonFile, Loan::class.java)
            "Account" -> mapper.readValue(jsonFile, Account::class.java)
            "Savings" -> mapper.readValue(jsonFile, SavingsAccount::class.java)
            "Investment" -> mapper.readValue(jsonFile, Investment::class.java)
            else -> throw IllegalArgumentException("Unknown account type: $accountType")
        }

        Mockito.`when`(georgeClient.getAccount(userId, accountNumber, bank)).thenReturn(expectedAccount as Account?)

        val actualAccount = georgeServiceImpl.getAccount(userId, accountNumber, bank)

        Assert.assertEquals(expectedAccount, actualAccount)
    }
}