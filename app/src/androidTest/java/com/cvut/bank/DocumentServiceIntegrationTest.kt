package com.cvut.bank

import android.content.ContentResolver
import android.content.Context
import android.net.Uri
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.cvut.bank.model.DocumentReference
import com.cvut.bank.service.DocumentService
import com.cvut.bank.service.UserService
import com.google.android.gms.tasks.Tasks
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import org.junit.Assert
import org.junit.Assert.assertNotNull
import org.junit.Assert.assertTrue
import org.junit.Assert.fail
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit

@RunWith(AndroidJUnit4::class)
class DocumentServiceIntegrationTest {
    private lateinit var auth: FirebaseAuth
    private lateinit var db: FirebaseFirestore
    private lateinit var userService: UserService
    private lateinit var documentService: DocumentService

    @Before
    fun setUp() {
        auth = FirebaseAuth.getInstance()
        db = FirebaseFirestore.getInstance()
        userService = UserService(ApplicationProvider.getApplicationContext())
        userService.setAuth(auth)
        userService.setDb(db)

        documentService = DocumentService(ApplicationProvider.getApplicationContext())
    }

    @Test
    fun testLoadUserDocuments() {
        val email = "testuser@example.com"
        val password = "testpassword"

        val taskLogin = userService.loginUser(email, password)
        Tasks.await(taskLogin)

        val testUserId = userService.getAuthenticatedUserId()

        val latch = CountDownLatch(1)
        var documentsList: List<DocumentReference>? = null

        testUserId?.let { userId ->
            documentService.loadUserDocuments(userId, { documents ->
                // onSuccess
                documentsList = documents
                latch.countDown()
            }, { exception ->
                // onFailure
                fail(exception.message)
            })
        }

        latch.await(10, TimeUnit.SECONDS)
        assertNotNull(documentsList)
        assertTrue(documentsList!!.isNotEmpty())
    }

    @Test
    fun testUploadFileToFirebaseStorage() {
        val email = "Pepik@gmail.com"
        val password = "Heslo.123456"

        val taskLogin = userService.loginUser(email, password)
        Tasks.await(taskLogin)

        val testUserId = userService.getAuthenticatedUserId()

        val assetManager = ApplicationProvider.getApplicationContext<Context>().assets
        val testFilePath = "testDocs/testFile.pdf"
        val testFileInputStream = assetManager.open(testFilePath)
        val testFileUri = Uri.parse(ContentResolver.SCHEME_CONTENT + "://" + testFilePath)

        val latch = CountDownLatch(1)
        var downloadUri: Uri? = null

        testUserId?.let { userId ->
            documentService.uploadFileToFirebaseStorage(userId, testFileUri, { uri ->
                // onSuccess
                downloadUri = uri
                latch.countDown()
            }, { exception ->
                // onFailure
                fail(exception.message)
            })
        }


        latch.await(10, TimeUnit.SECONDS)
        assertNotNull(downloadUri)
    }
}