package com.cvut.bank

import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.cvut.bank.model.Account
import com.cvut.bank.model.User
import com.cvut.bank.service.KBServiceImpl
import com.cvut.bank.service.UserService
import com.google.android.gms.tasks.Task
import com.google.android.gms.tasks.Tasks
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class UserServiceIntegrationTest {

    private lateinit var auth: FirebaseAuth
    private lateinit var userService: UserService

    @Before
    fun setUp() {
        auth = FirebaseAuth.getInstance()
        userService = UserService(ApplicationProvider.getApplicationContext())
    }


    @Test
    fun testCreateUserIntegration() {
        val name = "John"
        val surname = "Doe"
        val email = "testuser1@example.com"
        val password = "testpassword"
        val task = userService.createUser(name, surname, email, password)
        Tasks.await(task)
        assertTrue(task.isSuccessful)

        val userId = auth.currentUser?.uid
        val userTask = userService.getUser(userId!!)
        val userDocument = Tasks.await(userTask)

        assertTrue(userTask.isSuccessful)
        assertEquals(name, userDocument.getString("name"))
        assertEquals(surname, userDocument.getString("surname"))
        assertEquals(email, userDocument.getString("email"))
    }

    @Test
    fun testLoginUserIntegration() {
        val name = "John"
        val surname = "Doe"
        val email = "testuser2@example.com"
        val password = "testpassword"
        val task = userService.createUser(name, surname, email, password)
        Tasks.await(task)
        assertTrue(task.isSuccessful)

        val task2 = userService.loginUser(email, password)
        Tasks.await(task2)

        assertTrue(task2.isSuccessful)
        assertNotNull(auth.currentUser)
    }

    @Test
    fun testGetAuthenticatedUserId() {
        val name = "Jane"
        val surname = "Doe"
        val email = "testuser3@example.com"
        val password = "testpassword"
        val task = userService.createUser(name, surname, email, password)
        Tasks.await(task)
        assertTrue(task.isSuccessful)


        val loginTask = userService.loginUser(email, password)
        Tasks.await(loginTask)

        val userId = userService.getAuthenticatedUserId()
        assertNotNull(userId)
        assertEquals(auth.currentUser?.uid, userId)
    }

    @Test
    fun testGetUserAccounts() {
        val email = "Pepik@gmail.com"
        val password = "Heslo.123456"

        val loginTask = userService.loginUser(email, password)
        Tasks.await(loginTask)

        val userId = auth.currentUser?.uid!!

        val accountNumber = "6511231111"
        val serviceProvider = "ČSOB"
        val addAccountTask = userService.addAccount(userId, accountNumber, serviceProvider)
        Tasks.await(addAccountTask)
        assertTrue(addAccountTask.isSuccessful)

        val kbServiceImpl = KBServiceImpl(ApplicationProvider.getApplicationContext())
        val account = kbServiceImpl.getAccount(userId, accountNumber, serviceProvider)
        val expectedAccounts = listOf(account)

        val accountsTask = userService.getUserAccounts()
        val accounts = Tasks.await(accountsTask)

        assertTrue(accountsTask.isSuccessful)
        assertNotNull(accounts)
        assertEquals(expectedAccounts, accounts.toObjects(Account::class.java))
    }

    @Test
    fun testUpdateUser() {
        val name = "John"
        val surname = "Doe"
        val email = "testuser5@example.com"
        val password = "testpassword"
        val task = userService.createUser(name, surname, email, password)
        Tasks.await(task)
        assertTrue(task.isSuccessful)

        val loginTask = userService.loginUser(email, password)
        Tasks.await(loginTask)

        val userId = auth.currentUser?.uid!!
        val userTask = userService.getUser(userId)
        val userDocument = Tasks.await(userTask)

        val user = userDocument.toObject(User::class.java)
        user?.name = "Jane Updated"

        val updateTask = userService.updateUser(user)
        Tasks.await(updateTask)

        assertTrue(updateTask.isSuccessful)

        val updatedUserTask = userService.getUser(userId)
        val updatedUserDocument = Tasks.await(updatedUserTask)

        assertTrue(updatedUserTask.isSuccessful)
        assertEquals("Jane Updated", updatedUserDocument.getString("name"))
    }

    @Test
    fun testUpdateUserAccountsFromBankApi() {
        val name = "John"
        val surname = "Doe"
        val email = "testuser6@example.com"
        val password = "testpassword"
        val task = userService.createUser(name, surname, email, password)
        Tasks.await(task)
        assertTrue(task.isSuccessful)

        val loginTask = userService.loginUser(email, password)
        Tasks.await(loginTask)

        val userId = auth.currentUser?.uid!!

        userService.updateUserAccountsFromBankApi(userId)

        val userTask = userService.getUser(userId)
        val userDocument = Tasks.await(userTask)

        assertTrue(userTask.isSuccessful)
        val user = userDocument.toObject(User::class.java)
        assertNotNull(user?.financialProducts)
    }
}

