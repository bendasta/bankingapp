package com.cvut.bank

import androidx.test.platform.app.InstrumentationRegistry
import com.cvut.bank.bankClients.CoinbaseClient
import com.cvut.bank.model.Account
import com.cvut.bank.model.FinancialProduct
import com.cvut.bank.model.Investment
import com.cvut.bank.model.Loan
import com.cvut.bank.model.SavingsAccount
import com.cvut.bank.service.CoinbaseServiceImpl
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class CoinbaseServiceImplIntegrationTest {
    @Mock
    private lateinit var coinbaseClient: CoinbaseClient

    private val context = InstrumentationRegistry.getInstrumentation().context

    private lateinit var coinbaseServiceImpl: CoinbaseServiceImpl

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        coinbaseServiceImpl = CoinbaseServiceImpl(context)
        coinbaseServiceImpl.setClient(coinbaseClient)
    }

    @Test
    fun testGetUserAccounts() {
        val userId = "VItzY9wTcTXQ5ZI2HkfFhoam0eA2"
        val mapper = jacksonObjectMapper()
        val jsonFile = context.assets.open("BanksAPIs/Coinbase/Users/$userId.json")
        val userData: Map<String, Any> = mapper.readValue(jsonFile)
        val expectedAccounts: List<Account> = (userData["accounts"] as List<Account>)
        Mockito.`when`(coinbaseClient.getUserAccounts(userId)).thenReturn(expectedAccounts)

        val actualAccounts = coinbaseServiceImpl.getUserAccounts(userId)

        Assert.assertEquals(expectedAccounts, actualAccounts)
    }

    @Test
    fun testGetAccount() {
        val userId = "VItzY9wTcTXQ5ZI2HkfFhoam0eA2"
        val accountNumber = "4593217563"
        val bank = "Coinbase"
        val mapper = jacksonObjectMapper()
        val jsonFile = context.assets.open("BanksAPIs/$bank/Accounts/$accountNumber.json").bufferedReader().use { it.readText() }
        val accountData: Map<String, Any> = mapper.readValue(jsonFile)
        val accountType = accountData["account_type"] as String

        val expectedAccount: FinancialProduct? = when (accountType) {
            "Loan" -> mapper.readValue(jsonFile, Loan::class.java)
            "Account" -> mapper.readValue(jsonFile, Account::class.java)
            "Savings" -> mapper.readValue(jsonFile, SavingsAccount::class.java)
            "Investment" -> mapper.readValue(jsonFile, Investment::class.java)
            else -> throw IllegalArgumentException("Unknown account type: $accountType")
        }

        Mockito.`when`(coinbaseClient.getAccount(userId, accountNumber, bank)).thenReturn(expectedAccount as Account?)

        val actualAccount = coinbaseServiceImpl.getAccount(userId, accountNumber, bank)

        Assert.assertEquals(expectedAccount, actualAccount)
    }
}