package com.cvut.bank

import androidx.test.platform.app.InstrumentationRegistry
import com.cvut.bank.bankClients.KbClient
import com.cvut.bank.model.Account
import com.cvut.bank.model.FinancialProduct
import com.cvut.bank.model.Investment
import com.cvut.bank.model.Loan
import com.cvut.bank.model.SavingsAccount
import com.cvut.bank.service.KBServiceImpl
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class KBServiceImplIntegrationTest {

    @Mock
    private lateinit var kbClient: KbClient

    private val context = InstrumentationRegistry.getInstrumentation().context

    private lateinit var kbServiceImpl: KBServiceImpl

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        kbServiceImpl = KBServiceImpl(context)
        kbServiceImpl.setClient(kbClient)
    }

    @Test
    fun testGetUserAccounts() {
        val userId = "VItzY9wTcTXQ5ZI2HkfFhoam0eA2"
        val mapper = jacksonObjectMapper()
        val jsonFile = context.assets.open("BanksAPIs/KB/Users/$userId.json")
        val userData: Map<String, Any> = mapper.readValue(jsonFile)
        val expectedAccounts: List<Account> = (userData["accounts"] as List<Account>)
        Mockito.`when`(kbClient.getUserAccounts(userId)).thenReturn(expectedAccounts)

        val actualAccounts = kbServiceImpl.getUserAccounts(userId)

        assertEquals(expectedAccounts, actualAccounts)
    }

    @Test
    fun testGetAccount() {
        val userId = "VItzY9wTcTXQ5ZI2HkfFhoam0eA2"
        val accountNumber = "1232556645"
        val bank = "KB"
        val mapper = jacksonObjectMapper()
        val jsonFile = context.assets.open("BanksAPIs/$bank/Accounts/$accountNumber.json").bufferedReader().use { it.readText() }
        val accountData: Map<String, Any> = mapper.readValue(jsonFile)
        val accountType = accountData["account_type"] as String

        val expectedAccount: FinancialProduct? = when (accountType) {
            "Loan" -> mapper.readValue(jsonFile, Loan::class.java)
            "Account" -> mapper.readValue(jsonFile, Account::class.java)
            "Savings" -> mapper.readValue(jsonFile, SavingsAccount::class.java)
            "Investment" -> mapper.readValue(jsonFile, Investment::class.java)
            else -> throw IllegalArgumentException("Unknown account type: $accountType")
        }

        Mockito.`when`(kbClient.getAccount(userId, accountNumber, bank)).thenReturn(expectedAccount as Account?)

        val actualAccount = kbServiceImpl.getAccount(userId, accountNumber, bank)

        assertEquals(expectedAccount, actualAccount)
    }
}
