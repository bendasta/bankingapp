package com.cvut.bank

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreSettings
import com.google.firebase.storage.FirebaseStorage

class FirestoreEmulatorHelper {

    private lateinit var db: FirebaseFirestore
    private lateinit var auth: FirebaseAuth
    private lateinit var storage: FirebaseStorage

    fun startFirestoreEmulator() {
        val firestoreEmulatorHost = "10.0.2.2"
        val firestoreEmulatorPort = 8080
        val authEmulatorHost = "10.0.2.2"
        val authEmulatorPort = 9099
        val storageEmulatorHost = "10.0.2.2"
        val storageEmulatorPort = 9199

        // Firestore
        db = FirebaseFirestore.getInstance()
        val firestoreSettings = FirebaseFirestoreSettings.Builder()
            .setHost("$firestoreEmulatorHost:$firestoreEmulatorPort")
            .setSslEnabled(false)
            .build()
        db.firestoreSettings = firestoreSettings

        // Authentication
        auth = FirebaseAuth.getInstance()
        auth.useEmulator(authEmulatorHost, authEmulatorPort)

        // Storage
        storage = FirebaseStorage.getInstance()
        storage.useEmulator(storageEmulatorHost, storageEmulatorPort)
    }

    fun getEmulatorFirestoreInstance(): FirebaseFirestore {
        return db
    }

    fun getEmulatorFirebaseAuthInstance(): FirebaseAuth {
        return auth
    }

    fun getEmulatorFirebaseStorageInstance(): FirebaseStorage {
        return storage
    }
}
