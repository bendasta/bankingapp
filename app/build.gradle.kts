plugins {
    alias(libs.plugins.androidApplication)
    alias(libs.plugins.jetbrainsKotlinAndroid)
    id("com.google.gms.google-services")
}

android {
    namespace = "com.cvut.bank"
    compileSdk = 34

    packaging {
        resources.excludes.add ("META-INF/LICENSE.md")
        resources.excludes.add ("META-INF/LICENSE.txt")
        resources.excludes.add ("META-INF/DEPENDENCIES")
        resources.excludes.add ("META-INF/LICENSE-notice.md")
    }

    buildFeatures {
        viewBinding = true
    }

    defaultConfig {
        applicationId = "com.cvut.bank"
        minSdk = 24
        targetSdk = 34
        versionCode = 1
        versionName = "1.0"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }
    aaptOptions {
        noCompress ("pdf")
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_11
        targetCompatibility = JavaVersion.VERSION_11
    }
    kotlinOptions {
        jvmTarget = "11"
    }
}

dependencies {
    implementation(libs.androidx.core.ktx)
    implementation(libs.androidx.appcompat)
    implementation(libs.material)
    implementation(libs.androidx.activity)
    implementation(libs.androidx.constraintlayout)
    implementation(libs.firebase.auth.ktx)
    implementation(libs.firebase.database)
    implementation(libs.firebase.auth)
    implementation(libs.javax.persistence.api)
    implementation(libs.firebase.messaging.ktx)
    implementation(libs.androidx.datastore.core.android)
    implementation(libs.androidx.annotation)
    implementation(libs.androidx.lifecycle.livedata.ktx)
    implementation(libs.androidx.lifecycle.viewmodel.ktx)
    implementation(libs.androidx.biometric.v120alpha03)
    implementation(platform(libs.firebase.bom))
    implementation(libs.firebase.analytics)
    implementation(libs.firebase.firestore.ktx)
    implementation(libs.jackson.databind)
    implementation(libs.jackson.module.kotlin)
    implementation(libs.firebase.storage.ktx)
    implementation (libs.glide)
    implementation (libs.play.services.tasks)
    testImplementation(libs.mockito.mockito.core)
    testImplementation(libs.junit)
    testImplementation (libs.mockito.mockito.core)
    testImplementation (libs.mockito.inline)
    annotationProcessor (libs.compiler)
    androidTestImplementation(libs.junit.jupiter)
    androidTestImplementation(libs.androidx.junit)
    androidTestImplementation(libs.androidx.espresso.core)
    androidTestImplementation(libs.mockito.mockito.core)
    androidTestImplementation (libs.mockito.mockito.android)

}